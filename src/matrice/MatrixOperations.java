package matrice;

import java.util.Scanner;

public class MatrixOperations {

    // Max value stores the largest product
    private int[] max = { 0 };
    private int i, j, n, m;
    private int matrix[][];
    // This array stores M consecutive elements with the largest product
    private int savedPositions[];

    public static void main(String[] args) {
        MatrixOperations matrix = new MatrixOperations();
        matrix.readMatrix();
        matrix.calculate();
        matrix.displayResult();
    }

    public void readMatrix() {
        System.out.println("Give matrix dimensions:");
        Scanner scanIn = new Scanner(System.in);
        n = scanIn.nextInt();
        System.out.println("Give product size:");
        m = scanIn.nextInt();
        matrix = new int[n][n];
        savedPositions = new int[m];
        // Test if m>n
        if (m > n)
            System.out.println("You must enter a number < matrix dimension");
        else {
            System.out.println("Give matrix");
            for (i = 0; i < n; i++)
                for (j = 0; j < n; j++)
                    matrix[i][j] = scanIn.nextInt();
            // Close scanner
            scanIn.close();
        }
    }

    public void displayResult() {
        // Printing the result
        System.out.println("Result is: ");
        for (i = 0; i < m; i++)
            System.out.println(savedPositions[i] + " ");
        System.out.println("The largest product is " + max[0]);

    }

    public void calculate() {
        // Calculate largest product by line
        for (i = 0; i < n; i++) {
            for (j = 0; j <= n - m; j++)
                calculateProductByLine(matrix, i, j, n, m, max, savedPositions);
        }
        // Calculate largest product by column
        for (i = 0; i <= n - m; i++)
            for (j = 0; j < n; j++)
                calculateProductByColumn(matrix, i, j, n, m, max,
                        savedPositions);
        // Calculate largest product by diagonal
        for (i = 0; i <= n - m; i++)
            for (j = 0; j < n; j++)
                calculateProductByDiagonal(matrix, i, j, n, m, max,
                        savedPositions);

    }

    // This function calculate the product on the same line
    public void calculateProductByLine(int[][] matrix, int i, int j, int n,
            int m, int[] max, int[] savedPositions) {
        int product = 1;
        int index;
        for (index = 0; index < m; index++)
            product = product * matrix[i][j + index];
        if (product > max[0]) {
            max[0] = product;
            for (index = 0; index < m; index++)
                savedPositions[index] = matrix[i][j + index];
        }
    }

    // This function calculate the product on the same column
    public void calculateProductByColumn(int[][] matrix, int i, int j, int n,
            int m, int[] max, int[] savedPositions) {
        int product = 1;
        int index;
        for (index = 0; index < m; index++)
            product = product * matrix[i + index][j];
        if (product > max[0]) {
            max[0] = product;
            for (index = 0; index < m; index++)
                savedPositions[index] = matrix[i + index][j];
        }
    }

    // This function calculate the product on the same diagonal
    public void calculateProductByDiagonal(int[][] matrix, int i, int j, int n,
            int m, int[] max, int[] savedPositions) {
        int productDiagLeft = 1, productDiagRight = 1;
        int index;
        // If the matrix has an odd dimension, the middle column is calculated
        if ((j == n / 2) && (n % 2 == 1) && (m < n / 2 + 1)) {
            for (index = 0; index < m; index++) {
                productDiagLeft = productDiagLeft
                        * matrix[i + index][j - index];
                productDiagRight = productDiagRight
                        * matrix[i + index][j + index];
            }
            if (productDiagLeft > max[0]) {
                max[0] = productDiagLeft;
                for (index = 0; index < m; index++)
                    savedPositions[index] = matrix[i + index][j - index];
            }
            if (productDiagRight > max[0]) {
                max[0] = productDiagRight;
                for (index = 0; index < m; index++)
                    savedPositions[index] = matrix[i + index][j + index];
            }
        }
        if (j < n / 2) {
            for (index = 0; index < m; index++)
                productDiagRight = productDiagRight
                        * matrix[i + index][j + index];
            if (productDiagRight > max[0]) {
                max[0] = productDiagRight;
                for (index = 0; index < m; index++)
                    savedPositions[index] = matrix[i + index][j + index];
            }
        }
        if ((j > n / 2) || ((j == n / 2) && (n % 2 == 0))) {
            for (index = 0; index < m; index++)
                productDiagLeft = productDiagLeft
                        * matrix[i + index][j - index];
            if (productDiagLeft > max[0]) {
                max[0] = productDiagLeft;
                for (index = 0; index < m; index++)
                    savedPositions[index] = matrix[i + index][j - index];
            }
        }
    }
}