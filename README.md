2. Given a NxN matrix. Find the largest product of M consecutive elements placed on the same line, column or diagonal.
 
Eg:
In the following 5x5 matrix, find the largest 3 factors product:
 
1 1 1 4 1
1 2 5 8 1
1 9 1 0 2
2 3 1 2 5
1 1 2 3 1
 
Expected result: 4 * 5 * 9 = 180



Exemplu rulare: 

Give matrix dimensions:
3
Give product size:
3
Give matrix
4 5 6
7 8 9
5 6 9
Result is: 
7 
8 
9 
The largest product is 504